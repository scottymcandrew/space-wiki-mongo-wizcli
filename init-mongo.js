db = new Mongo().getDB("space_definitions");

db.createCollection('users', {capped: false});
db.createCollection('definitions', {capped: false});

db.definitions.createIndex({"definition_name": "text", "definition": "text"})

db.createUser(
    {
        user: "space_user",
        pwd: "jupiter",
        roles: [
            { role: "readWrite", db: "space_definitions" }
        ]
    }
)

db.definitions.insert([
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Asteroid",
        "definition": "Rocks floating around in space. Some are the size of a pick-up truck. Others are hundreds of miles across."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Atmosphere",
        "definition": "The gases held by gravity around Earth and around other planets. Can also be used to talk about gases around stars."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Atom",
        "definition": "The basic building block of matter. It is made of protons"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Black Hole",
        "definition": "A place in space where matter and light cannot escape if they fall in."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Comet",
        "definition": "An icy rock that lets off gas and dust"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Constellation",
        "definition": "A group of stars in the sky. They're often named after an animal"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Corona",
        "definition": "The outer atmosphere of a star."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Cosmos",
        "definition": "The universe seen as an orderly"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Dwarf Planet",
        "definition": "Objects that are round and orbit the sun"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Exoplanet",
        "definition": "A planet that freely floats between stars or one that orbits a star outside our solar system."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Galaxy",
        "definition": "A collection of thousands to billions of stars held together by gravity. The galaxy we live in is called the Milky Way."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Gravity",
        "definition": "A force that pulls matter together."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Kuiper Belt",
        "definition": "A donut-shaped ring of icy objects beyond the orbit of Neptune. Pluto is the best known of these icy worlds."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Light Year",
        "definition": "It’s not a year"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Mass",
        "definition": "The amount of matter something is made of."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Meteor",
        "definition": "The streak of light caused when a meteoroid enters a planet’s atmosphere and starts to burn from the heat of friction."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Meteorite",
        "definition": "A meteoroid that lands on the surface of a planet."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Molecule",
        "definition": "The smallest unit of a substance that still acts like the main substance. A molecule can be a single atom or a group of atoms. Water is a substance"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Moon",
        "definition": "A natural object that travels around a bigger natural object. Planets can have moons. Dwarf planets can have moons. Even some asteroids have moons! Astronomers usually call them satellites or natural satellites."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Nebula",
        "definition": "A cloud of dust or gas found between stars."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Neutron Star",
        "definition": "A very dense star made mostly of neutrons. It has very powerful gravitational force nearby because the whole mass of a star is pulled into one object just a few miles across."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Oort Cloud",
        "definition": "A spherical shell around our solar system. It may contain more than a trillion icy bodies. Long-period comets (which take more than 200 years to orbit the sun) come from the Oort Cloud."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Orbit",
        "definition": "The curved path that a planet"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Ozone Layer",
        "definition": "A part of Earth’s atmosphere that absorbs lots of the sun’s ultraviolet radiation. It is made of a gas called ozone"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Particle",
        "definition": "A tiny amount or small piece of something."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Planet",
        "definition": "A large body in outer space that circles around the sun or another star."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Pulsar",
        "definition": "An object"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Quasar",
        "definition": "Compact area in the center of a massive galaxy that is around a supermassive black hole. They are some of the brightest objects in the universe and can be observed across the entire electromagnetic spectrum."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Radiation",
        "definition": "The energy or particles released from sources like radioactive materials"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Radio Waves",
        "definition": "Part of the electromagnetic spectrum. Radio waves are around us here on Earth"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Radioactive",
        "definition": "How we describe some atoms that are unstable. They change into different kinds of atoms and release lots of energy."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Satellite",
        "definition": "An object that orbits another object. A moon is actually a satellite. We also say satellite to refer to spacecraft people build that orbit Earth"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Solar Flare",
        "definition": "A burst of energy and particles from the sun. It releases gases"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Solar System",
        "definition": "A set that includes a star and all of the matter that orbits it"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Solar Wind",
        "definition": "The constant stream of particles and energy emitted by the sun."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Speed of Light",
        "definition": "Light is the fastest thing in the universe. It travels 186"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Star",
        "definition": "A ball of shining gas"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Sun",
        "definition": "The star in the center of our solar system."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Supermassive",
        "definition": "How we describe objects that have a million times (or more!) mass than our sun."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Supernova",
        "definition": "The explosion of a star that makes it as bright as a whole galaxy."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Ultraviolet",
        "definition": "Part of the electromagnetic spectrum. It’s called ultraviolet because the waves are shorter than violet light. We can’t see ultraviolet light with our eyes"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Universe",
        "definition": "All of space and time"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Vacuum",
        "definition": "An empty space that doesn't have any matter."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Visible Light",
        "definition": "The part of the electromagnetic spectrum that we can see with our eyes. It’s all the colors of the rainbow."
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "Wave",
        "definition": "A way energy moves from one place to another. Sometimes waves move materials the way water ripples in a pond move the water. Other times"
    },
    {
        "author": "Scotty",
        "date": ISODate("2021-09-07 20:43:28.275403"),
        "votes": 1,
        "top_definition": true,
        "definition_name": "X-Rays",
        "definition": "Radiation with lots of energy made by very hot gases"
    }
]);